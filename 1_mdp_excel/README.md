# Mot de Passe Excel

Le fichier Excel `Perso.xlsx` est chiffré et protégé par un mot de passe à première vue impossible à casser.

## Recherche d'indications

En explorant les différents fichiers, nous avons trouvé la chaîne '06081967+CB' dans un des fichiers audio (`Musique/Années 80 - Queen - We will rock you my secret.mp3`). Nous avons donc déduit que le mot de passe serait sûrement composé de '06081967' suivi d'un code de carte bancaire.

## Résolution du mot de passe

Nous avons écrit le script python `makewordlist.py` qui permet de générer le fichier `wordlist` contenant toutes les combinaisons possibles.

L'utilitaire JohnTheRipper propose de brute-forcer un fichier Excel de la façon suivante :

- On crée le fichier `hash` avec `office2john.py` (dans le dossier run de JohnTheRipper) :
`office2john.py Perso.xlsx > hash`

- On lance JohnTheRipper contre le fichier créé, avec notre liste de combinaisons :
`john --rules --wordlist=wordlist hash`

Après 12 secondes, le mot de passe `060819677940` est trouvé.
