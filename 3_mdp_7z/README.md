# Mot de passe archive

L'archive présente dans le dossier `Photos` de la clé USB est protégée par un mot de passe.

De la même façon que pour le fichier Excel, nous utiliserons JohnTheRipper pour essayer de casser le mot de passe.

La difficulté ici réside dans le manque d'informations ou d'indice concernant le mot de passe, contrairement à celui de l'Excel qui était indiqué dans un des fichiers audio.

Nous allons donc essayer de se débrouiller avec les informations dont nous disposons.

Le script python `makewordlist.py` génère donc toutes les combinaisons possibles des informations contenues dans la feuille 2 du fichier Excel (Le code de carte bancaire, le code PIN et les coordonnées bancaires) et les stocke dans `wordlist`.

On génère ensuite le fichier `hash`à l'aide du script perl `7ztojohn.pl` (dans le dossier run de JohnTheRipper) : 
`7z2john.pl Photos.7z > hash`

On lance ensuite JohnTheRipper contre ce fichier, avec notre liste de combinaisons :
`john --rules --wordlist=wordlist hash`

Notre ami John trouve très vite le mot de passe qui s'avère être `79402222` !
