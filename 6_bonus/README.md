# Bonus

Une analyse approfondie des fichiers met en lumière un certain nombre de choses...

## Suspicions

Dès notre première lecture du sujet, nous n'avons pu nous empêcher de remarquer l'année de naissance de M. Dupont : 1967.

Il n'échappe à personne qu'en lui appliquant la fonction quantique suprême* on tombe inexorablement sur le chiffre du Diable : **666**.

\* f: x → √2 * (√(x²) - x) + x - 1301

Forts de cette trouvaille, nous avons décidé de poursuivre nos recherches afin de mettre en évidence les complots en jeu dans cette affaire.

## Coordonnées géographiques

Le tracé des déplacements de M. Dupont apparait d'abord tout à fait normal. Mais en faisant appel à son troisième oeil chamanique, on découvre alors la supercherie. Il se trouve qu'en augmentant le contraste et en appliquant une fonction gaussienne au tracé apparait un oeil sacré Illuminati

![photo](../images/illuminati.jpg "Illuminati")

## Conclusion

Nous avons donc de bonnes raisons de croire que M. Dupont est impliqué dans de nombreux complots interplanétaires, ce qui viendra s'ajouter à ses précédents délits.
