# Exploration des photos

Les photos contenues dans l'archive n'ont à première vue aucune valeur pour l'enquête.
On observe cependant que chacune contient des coordonnées GPS. Essayons d'en faire quelque chose...

## Récupération des coordonnées

On les récupère et génère un fichier trace GPX via le script python `generate_gpx.py` : 
`python generate_gpx.py Photos/Photos`

Après un premier essai, on se rend compte que les photos ne sont pas ordonnées par date et donc que la trace générée n'a pas beaucoup de sens... (On observe toutefois une grande densité de points en Thaïlande, en Scandinavie et dans les îles Canaries)

On reprendra donc le script pour prendre en compte la date de création des photos et ordonner la trace en conséquence.

On peut donc maintenant explorer le fichier `trace.gpx` dans n'importe quelle application de cartes pour découvrir ce que l'on pense être l'historique des déplacements de notre suspect.

## Exploitation des coordonnées

Après affichage dans Google maps, voici le résultat :

![Carte des déplacements](../images/historique_deplacements.png "Carte des déplacements")

| Date             | Lieu                                                                                  | Photo           |
|------------------|---------------------------------------------------------------------------------------|-----------------|
| 25 Mai 2014      | Dawn Dives Academy - Playa Blanca, Las Palmas, Espagne                                | voyage (2).JPG  |
| 26 Août 2014     | Jernbanetorget tram stop - Oslo, Norvège                                              | voyage (3).JPG  |
| 26 Août 2014     | Inwester AS - Oslo, Norvège                                                           | voyage (4).JPG  |
| 26 Août 2014     | Holmenkollen Ski Museum - Oslo, Norvège                                               | voyage (5).JPG  |
| 26 Août 2014     | Nedre Korskirkeallmenningen - Bergen, Norvège                                         | voyage (6).JPG  |
| 26 Août 2014     | Sjøboden Bergen - Bergen, Norvège                                                     | voyage (7).JPG  |
| 28 Août 2014     | Vejers Strand - Vejers, Danemark                                                      | voyage (8).JPG  |
| 1 Septembre 2014 | Night Church - København, Danemark                                                    | voyage (9).JPG  |
| 1 Septembre 2014 | Storebæltsbroen - Korsør, Danemark                                                    | voyage (10).JPG |
| 1 Septembre 2014 | Moss Copenhagen - Vejle, Danemark                                                     | voyage (11).JPG |
| 23 Octobre 2014  | Au milieu de River Colne - London, Angleterre                                         | voyage (12).JPG |
| 24 Octobre 2014  | Sofitel Bangkok Sukhumvit -  Bangkok, Thaïlande (A côté d'une agence Western Union)   | voyage (13).JPG |
| 25 Octobre 2014  | Ruen Kanok Thai House - Chang Wat Prachuap Khiri Khan, Thaïlande                      | voyage (14).JPG |
| 29 Octobre 2014  | Immigration Checkpoint - Thakhek, Laos (A côté du SKKS Club)                          | voyage (16).JPG |
| 1 Novembre 2014  | Chao Phraya River - Bangkok, Thaïlande                                                | voyage (1).JPG  |
| 3 Novembre 2014  | Au beau milieu d'un champ (certainement de drogue) - Près de Nakhon Phanom, Thaïlande | voyage (15).JPG |

Certains emplacements sont suspects car ils ne correspondent pas à la photo, notamment le dernier indiquant un champ alors que la photo montre un petit aéroport vu d'un avion au sol, ce peut être dû à la mauvaise réception GPS dans la région.
